import prisma from '../_prisma' // prisma client
import { checkToken } from '../_auth' // auth mechanism
import { parse } from 'uuid';

export default async (req:any, res:any) => {
    const body = req.body;
    const query = req.query;
    const method = req.method;
    const headers = req.headers;
    
    if (!prisma[query.model]) {
        res.json({
            succes:false,
            method:method,
            err:'Model not found!'
        })
    } else {
        const check = await checkToken(headers.authorization)
        if (!check) {
            return res.status(403).json({
                succes:false,
                method:method,
                err:'Invalid Token!'
            })
        }
        switch (method){
            case 'OPTIONS':
                res.status(200).send('')
                break
            case 'GET': // GET REQUEST - READ
                try {
                    let options: any = {} // prisma options init
                    let where: any = {} // Conditions
                    let take: number = 30 // Default to 30 records
                    let skip: number = 0 // First page start with 0
                    let page: number = 1 // First page
                    let pages: number = 1 // Number of pages
                    let success: boolean = true // Se resulta em dados
                    if (query.take) {
                        take = parseInt(query.take)
                        options.take = take // take option
                    }
                    if (query.skip) {
                        skip = parseInt(query.skip)
                        options.skip = skip // skip option
                    }
                    
                    // Include relations
                    if (query.include) { // include option
                        const include = JSON.parse(query.include)
                        options.include = {...include}
                    }
                    // Filter query
                    if (query.where) {
                        where = JSON.parse(query.where)
                        options.where = {...where}
                    }
                    // Sort query
                    if (query.orderBy) {
                        const orderBy = JSON.parse(query.orderBy)
                        options.orderBy = {...orderBy}
                    }
                    // Send request
                    let count = await prisma[query.model].count({
                        where:where
                    })
                    pages = Math.ceil(count/take)
                    page = Math.ceil(skip/take+1)
                    let ret = [] // init empty array return variable
                    if (page > pages) {
                        page = pages
                        skip = (pages-1) * take
                        options.skip = skip
                    }
                    let data = await prisma[query.model].findMany(options) // array of data from model with options
                    // remove some fields from result
                    if (query.model=='user') {
                        data.map((d)=>{
                            delete d.password // user.password should not be listed
                            ret.push(d)
                        })
                    }
                    // return json with data
                    return res.status(200).json({
                        succes:success, // status
                        method:method, // method used
                        data:ret, // data
                        page:page,
                        pages:pages,
                        count:count // count for pagination
                    })
                } catch(err){
                    console.log(err)
                    return res.status(500).json({ // server error 500
                        succes:false,
                        method:method,
                        err:err
                    })
                }
                break
            case 'POST': // POST REQUEST - Create
                try {
                    const data = await prisma[query.model].create({data:body}) // create record from body data
                    // return json result
                    return res.json({
                        succes:true,  //status
                        method:method, // method
                    })
                } catch(err){
                    console.log(err)
                    return res.status(500).json({ // server error 500
                        succes:false,
                        method:method,
                        err:err
                    })
                }
                break
            default: // methods not implemented
                return res.json({
                    succes:false, // status
                    method:method, // method
                    err:'Method not implemented!' // error
                })
        }
    } 
}