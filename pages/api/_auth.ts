import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient({
    errorFormat: 'minimal',
  });

/**
 * 
 * @param {*} token 
 * @param {*} role 
 * @returns false or token/user
 * @description Recebe o token enviado na requisicao, consulta na tabela de usuarios e devolve true se encontrado ou false se não encontrado
 * se passado um parametros role, verifica se o usuario do token tem essa role
 */  
const checkToken = async function (token: string, role: any=undefined) {
    if (!token)
        return false
    // Find passed token
    const check = await prisma.token.findFirst({
        where: {
            token: {
                equals: token.split(' ')[1]
            }
        },
        include:{
            user:{
                select:{
                    id:true,
                    createdAt:true,
                    email:true,
                    name:true,
                    role:true
                }
            }
        }
    })
    if (check) { // return token
        return check
    } else
        return false // token não encontrado
}

export { checkToken }
